pub type Result<T> = ::std::result::Result<T, Error>;
use opts::Kind;
use std::{error, fmt};

#[derive(Debug, PartialEq, Eq, Clone)]
/// An error dealing with an option
pub enum Error {
    /// An unimplemented but valid option
    Unimplemented(Kind, &'static str),
    /// An unknown (and unsupported) option
    Unknown(Kind, String),
    /// An error parsing a numeric option
    ParseInt(std::num::ParseIntError),
    /// The directory flag is specified, but the target is not a directory.
    NotDirectory(String),
    /// Two or more mutually exclusive options have been selected.
    ConflictingOption(&'static str),
}

impl From<std::num::ParseIntError> for Error {
    fn from(err: std::num::ParseIntError) -> Self { Error::ParseInt(err) }
}
impl error::Error for Error {}

impl fmt::Display for Kind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Kind::BasicOption => write!(f, "basic option"),
            Kind::CFlag => write!(f, "conversion flag"),
            Kind::IFlag => write!(f, "iflag"),
            Kind::OFlag => write!(f, "oflag"),
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::Unknown(kind, arg) => write!(f, "unknown {} option \"{}\"", kind, arg),
            Error::Unimplemented(kind, arg) => write!(f, "unimplemented {} option \"{}\"", kind, arg),
            Error::ParseInt(err) => err.fmt(f),
            Error::NotDirectory(s) => write!(
                f,
                "flag 'directory' was specified for path {}, but it was not a directory",
                s
            ),
            Error::ConflictingOption(s) => write!(f, "conflicting options: {}", s),
        }
    }
}
