use super::{Kind, Result, Unimplemented};
use opts;
bitflags!{
    /// Output (write) options
    /// - append:  append mode (makes sense only for output; conv=notrunc
    /// suggested)
    ///
    /// - direct:  use direct I/O for data
    ///
    /// - directory:  fail unless a directory
    ///
    /// - dsync:  use synchronized I/O for data
    ///
    /// - sync   likewise, but also for metadata
    ///
    /// - nonblock:  use non-blocking I/O
    ///
    /// - noatime:  do not update access time
    ///
    /// - nocache:  Request to drop cache.  See also oflag=sync
    ///
    /// - noctty:  do not assign controlling terminal from file
    ///
    /// - nofollow:  do not follow symlinks
    ///
    /// - seek_bytes: treat 'seek=N' as a byte count (oflag only)
    ///
    ///
    pub struct OFlag: u32 {
        /// append mode (makes sense only for output; conv=notrunc suggested)
        const APPEND = 0x0001;
        /// use direct I/O for data
        const DIRECT = 0x0002;
        /// fail unless a directory
        const DIRECTORY = 0x0004;
        /// use synchronized I/O for data
        const DSYNC = 0x0008;
        /// likewise, but also for metadata
        const SYNC = 0x0010;
        /// use non-blocking I/O
        const NONBLOCK = 0x0020;
        /// do not update access time
        const NOATIME = 0x0040;
        /// Request to drop cache.  See also oflag=sync
        const NOCACHE = 0x0080;
        /// do not assign controlling terminal from file
        const NOCTTY = 0x0100;
        /// do not follow symlinks
        const NOFOLLOW = 0x0200;
    }
}
impl OFlag {
    /// create a new `flags::OFlag` by parsing a comma-and-optional-whitespace
    /// separated list of arguments
    /// ```
    /// # use dd_lib::opts::OFlag;
    /// let want = OFlag::SYNC | OFlag::APPEND;
    /// assert_eq!(want, OFlag::new("sync,  append  ").unwrap());
    /// ```
    pub fn new<S: AsRef<str>>(s: S) -> Result<Self> { s.as_ref().parse() }

    fn known_arg(flag: &str) -> Result<Self> {
        Ok(match flag {
            "append" => Self::APPEND,
            "sync" => Self::SYNC,
            "direct" => Self::DIRECT,
            "directory" => Self::DIRECTORY,
            "dsync" => Self::DSYNC,
            "nonblock" => Self::NONBLOCK,
            "noatime" => Self::NOATIME,
            "nocache" => Self::NOCACHE,
            "noctty" => Self::NOCTTY,
            "nofollow" => Self::NOFOLLOW,
            f => return Self::unknown(f.to_owned()),
        })
    }
}

impl Default for OFlag {
    fn default() -> OFlag { OFlag::empty() }
}

impl std::str::FromStr for OFlag {
    type Err = opts::Error;

    fn from_str(s: &str) -> Result<Self> {
        let mut flags = Self::default();

        for flag in s.split(',').map(str::trim).filter(|s| s.len() > 0) {
            Self::check_if_implemented(flag)?;
            flags |= Self::known_arg(flag)?;
        }
        Ok(flags)
    }
}

impl Unimplemented for OFlag {
    const KIND: Kind = Kind::OFlag;
    const UNIMPLEMENTED: &'static [&'static str] = &[
        "direct",
        "directory",
        "dsync",
        "nonblock",
        "noatime",
        "nocache",
        "noctty",
        "nofollow",
    ];
}
