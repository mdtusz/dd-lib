//! SI Units.
//! - **B**: 1
//! - **K**: 1024
//! - **M**: 1024²
//! - **G**: 1024³
//! - **T**: 1024⁴
//! - **P**: 1024⁵
//! - **Z**: 1024⁶
//! - **KB**: 10⁶
//! - **MB**: 10⁹
//! - **GB**: 10¹²
//! - **TB**: 10¹⁵
//! - **ZB**: 10¹⁸

/// 10³
pub const KB: usize = 1000;
/// KB*KB = 10⁶
pub const MB: usize = KB * KB;
/// MB*KB = 10⁹
pub const GB: usize = MB * KB;
/// GB*KB = 10¹²
pub const TB: usize = GB * KB;
/// GB*KB = 10¹⁵
pub const PB: usize = TB * KB;
/// TB*KB = 10¹⁸
pub const ZB: usize = PB * KB;

// pub const YB: usize = ZB * KB;

/// 512
pub const B: usize = 512;
/// 1024
pub const K: usize = 1024;
/// K*K = 1024²
pub const M: usize = K * K;
/// M*K = 1024³
pub const G: usize = M * K;
/// G*K = 1024⁴
pub const T: usize = G * K;
/// T*K = 1024⁵
pub const P: usize = T * K;
/// P*K = 1024⁶
pub const Z: usize = P * K;
// pub const Y: usize = Z * K;

/// BITSIZE is the pointer width of the target system
#[cfg(target_pointer_width = "64")]
/// (64)
pub const BITSIZE: usize = 64;
#[cfg(target_pointer_width = "32")]
/// (32)
pub const BITSIZE: usize = 32;
